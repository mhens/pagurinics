#ifndef PRDATAPUBLICDEFINITION_H
#define PRDATAPUBLICDEFINITION_H
#include "prgeneric.h"
#include "prdatagroup.h"
#include "prdatanodesgroup.h"
#include "prdataobligationsgroup.h"
#include "prdataobligationsnodesgroup.h"

typedef struct prdatapublicdefinition{
  uint32_t pr_map;
  PRDataGroup * pr_t_datagroup;
  PRDataNodesGroup * pr_t_datanodesgroup;
  PRDataObligationsGroup * pr_t_dataobligationsgroup;
  PRDataObligationsNodesGroup * pr_t_dataobligationsnodesgroup;
  unsigned char * pr_content;
}PRDataPublicDefinition;

#endif
