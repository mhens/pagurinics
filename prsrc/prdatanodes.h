#ifndef PRDATANODES
#define PRDATANODES
#include "prgeneric.h"
#include "prdata.h"

typedef struct prdatanodes{
  uint16_t pr_s_map;
  PRData * pr_s_owned;
  unsigned char * pr_s_identifier;
  unsigned char * pr_s_inner_value;
}PRDataNodes;

PRDataNodes * initPRDataNodes(void);
PRDataNodes * newPRDataNodes(uint16_t, PRData *, unsigned char *, unsigned char *);

#endif
