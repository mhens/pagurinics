#ifndef PRSIMPLEMEM_H
#define PRSIMPLEMEM_H
#include "prgeneric.h"

typedef struct prsimplemem{
  uint32_t pr_m_map;
  unsigned char ** pr_data_name;
}PRSimpleMem;

#endif
