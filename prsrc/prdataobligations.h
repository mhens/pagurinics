#ifndef PRDATAOBLIGATIONS_H
#define PRDATAOBLIGATIONS_H
#include "prgeneric.h"
#include "prdata.h"
#include "prdataobligationsnodes.h"

typedef struct prdataobligations{
  uint16_t pr_e_map;
  PRData * pr_e_owned;
  unsigned char * pr_e_identifier;
  uint8_t pr_e_len_obligations_nodes;
  PRDataObligationsNodes * pr_e_nodes;
}PRDataObligations;

PRDataObligations * initPRDataObligations(void);
PRDataObligations * newPRDataObligations(uint16_t, PRData *, unsigned char *, uint8_t, PRDataObligationsNodes *);

#endif
