#ifndef PRDATA_H
#define PRDATA_H
#include "prgeneric.h"
#include "prdatanodes.h"
#include "prdataobligations.h"

typedef struct prdata{
  uint16_t pr_map;
  PRData * pr_father;
  unsigned char * pr_identifier;
  uint8_t pr_len_nodes;
  PRDataNodes * pr_t_nodes;
  uint8_t pr_len_obligations;
  PRDataObligations * pr_t_obligations;
}PRData;

PRData * initPRData(void);
PRData * newPRData(uint16_t, PRData *, unsigned char *, uint8_t, PRDataNodes *, uint8_t, PRDataObligations *);

#endif
