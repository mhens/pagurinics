#ifndef PRDATAPUBLICDEFINITIONGROUP_H
#define PRDATAPUBLICDEFINITIONGROUP_H
#include "prgeneric.h"
#include "prdatapublicdefinition.h"

typedef struct prdatapublicdefinitiongroup{
  uint32_t pr_a_len;
  uint32_t * pr_a_map;
  PRDataPublicDefinition ** pr_key_data;
}PRDataPublicDefinitionGroup;

#endif
