#ifndef PRDATAOBLIGATIONSNODESGROUP_H
#define PRDATAOBLIGATIONSNODESGROUP_H
#include "prgeneric.h"
#include "prdataobligationsnodes.h"

typedef struct prdataobligationsnodesgroup{
  uint16_t pr_x_len;
  uint16_t * pr_x_map;
  PRDataObligationsNodes ** pr_key_data;
}PRDataObligationsNodesGroup;

#endif
