#ifndef PRDATAOBLIGATIONSGROUP_H
#define PRDATAOBLIGATIONSGROUP_H
#include "prgeneric.h"
#include "prdataobligations.h"

typedef struct prdataobligationsgroup{
  uint16_t pr_x_len;
  uint16_t * pr_x_map;
  PRDataObligations ** pr_key_data;
}PRDataObligationsGroup;

#endif
