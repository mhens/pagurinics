#ifndef PRDATAGROUP_H
#define PRDATAGROUP_H
#include "prgeneric.h"
#include "prdata.h"

tpedef struct prdatagroup{
  uint16_t pr_x_len;
  uint16_t * pr_x_map;
  PRData ** pr_key_data;
}PRDataGroup;

#endif
