#ifndef PRDATANODESGROUP_H
#define PRDATANODESGROUP_H
#include "prgeneric.h"
#include "prdatanodes.h"

typedef struct prdatanodesgroup{
  uint16_t pr_x_len;
  uint16_t * pr_x_map;
  PRDataNodes ** pr_key_data;
}PRDataNodesGroup;

#endif
