#ifndef SIMPLEMEMGROUP_H
#define SIMPLEMEMGROUP_H
#include "prgeneric.h"
#include "prsimplemem.h"

typedef struct prsimplememgroup{
  uint32_t pr_s_len;
  uint32_t * pr_s_map;
  PRSimpleMem ** pr_data_key;
}PRSimpleMemGroup;

#endif
