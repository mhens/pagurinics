#ifndef PRSIMPLEMEMCALL_H
#define PRSIMPLEMEMCALL_H
#include "prgeneric.h"
#include "prsimplememgroup.h"
#include "prdatapublicdefinitiongroup.h"

typedef struct prsimplememcall{
  PRSimpleMemGroup * pr_mem_lib;
  PRDataPublicDefinitionGroup * pr_exec_lib;
}PRSimpleMemCall;

#endif
