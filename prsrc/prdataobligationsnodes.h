#ifndef PRDATAOBLIGATIONSNODES_H
#define PRDATAOBLIGATIONSNODES_H
#include "prgeneric.h"
#include "prdataobligations.h"

typedef struct prdataobligationsnodes{
  uint16_t pr_s_map;
  PRDataObligations * pr_s_owned;
  unsigned char * pr_s_identifier;
  unsigned char * pr_s_inner_value;
}PRDataObligationsNodes;

PRDataObligationsNodes * initPRDataObligationsNodes(void);
PRDataObligationsNodes * newPRDataObligationsNodes(uint16_t, PRDataObligations *, unsigned char *, unsigned char *);

#endif
